import java.util.*;

/** Word puzzle.
 * @since 1.8
 */
public class Puzzle {

   private String[] originalStringArray = new String[]{};
   private HashMap<Character, Integer> uniqueLetters = new HashMap<>();
   private HashMap<Character, Integer> binaryDict = new HashMap<>();
   private List<Character> letterList = new ArrayList<>();
   private int size;
   private final int maxDigit = 9;

   /**
    * Get unique character sets from given three strings.
    * @param stringArr string array, must contain three strings.
    * @return dictionary formed letters. if a letter appears in the first place, the value must be bigger than 0.
    */
   Puzzle (String[] stringArr) {
      if (stringArr.length != 3) throw new IllegalArgumentException("Array should contain three strings. but " + stringArr.length + " given.");

      for (int i=0; i < 3; i++) {
         String str = stringArr[i];
         for (int j=0; j < str.length(); j++) {
            uniqueLetters.put(str.charAt(j), 0);
            if (j==0) {
               binaryDict.put(str.charAt(j), 1);
            } else {
               binaryDict.put(str.charAt(j), 0);
            }
         }
      }

      size = uniqueLetters.size();
      letterList = uniqueLetters.keySet().stream().toList();
      originalStringArray = stringArr;
   }

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {

//      String[] test = {"AB", "AB", "CB"};
//      String[] test = {"SEND", "MORE", "MONEY"};
      String[] test = {"YKS", "KAKS", "KOLM"};
      for (String s: args) {
         System.out.print(s + " ");
      }
      System.out.println();
      Puzzle p = new Puzzle(args);
      List<Hashtable<Character, Integer>> solutions = p.solveWordPuzzle();


   }

   public boolean validateAnswer(Hashtable<Character, Integer> solution) {

      List<Integer> numberArray = new ArrayList<Integer>();

      //construct integer list from string list
      for (int i=0;i<3;i++) {
         String str = originalStringArray[i];
         int letterNumber = 0;
         int strLen = str.length();
         for (int j = 0; j < strLen; j++) {
            int num = (int) Math.pow(10, strLen - j - 1) * solution.get(str.charAt(j));
            letterNumber += num;
         }
         numberArray.add(letterNumber);
      }

      //validate answer
      if (numberArray.get(0) + numberArray.get(1) == numberArray.get(2)) {
//         System.out.println(numberArray.get(0) + " + " + numberArray.get(1) + " = " + numberArray.get(2));
         System.out.println(solution);
         return true;
      }
      return false;
   }

   /**
    * Unique check
    * return boolean, true if all the digits are unique, otherwise false.
    */
   private static boolean isAllUnique(int[] list) {
      List<Integer> temp = new ArrayList<>();
      for (int i=0;i<list.length;i++) {
         temp.add(list[i]);
      }
      Set<Integer> set = new HashSet<Integer>(temp);
      if (set.size() == list.length) return true;
      return false;
   }

   /**
    * if letter contains unrealistic number, for example MONEY -> M cannot be zero.
    * check binaryDict and if
    * return boolean, true if all the digits are unique, otherwise false.
    */
   private boolean binaryCheck(int[] list) {
      for (int i=0; i<size;i++) {
         if (binaryDict.get(letterList.get(i)) == 1 && list[i] == 0) {
            return false;
         }
      }
      return true;
   }

   /**
    * Check given list is following the conditions.
    * @param list of int
    * @return boolean true if the list follows the conditions, otherwise false.
    */
   private  boolean checkConditions(int[] list) {

      if (!isAllUnique(list)) return false;
      if (!binaryCheck(list)) return false;

      return true;
   }

   /**
    * Converting list of int to dictionary of letter and integer pair.
    * @param list of int
    * @return {Character: Integer} pair.
    */
   private Hashtable<Character, Integer> constructDictionaryFromList(int[] list) {
      Hashtable<Character, Integer> dict = new Hashtable<>(uniqueLetters);
      for (int i=0; i<size;i++) {
         dict.put(letterList.get(i), list[i]); //set number by letter key
      }
      return dict;
   }

   /**
    * Function to find possible digit combination from given string array.
    * @return list of letter and integer pair.
    */
   public List<Hashtable<Character, Integer>> solveWordPuzzle() {


      List<Hashtable<Character, Integer>> solutions = new ArrayList<Hashtable<Character, Integer>>();
      System.out.println(binaryDict);System.out.println();


      int[] list = new int[size];
      for (int i=0;i<size;i++) {
         list[i] = binaryDict.get(letterList.get(i)) -1;
      }
      Hashtable<Character, Integer> dict = new Hashtable<Character, Integer>();

      int numSolutions = 0;
      int currentLetterIndex = 0;
      double start = System.currentTimeMillis();

      while (currentLetterIndex >=0) {
         if (list[currentLetterIndex] < maxDigit) {
            list[currentLetterIndex]++;
               if (currentLetterIndex < list.length-1) {
                  currentLetterIndex++;
               } else {
                  if (checkConditions(list)) {
                     dict = constructDictionaryFromList(list);
                     if (validateAnswer(dict)) {
                        solutions.add(dict);
                        numSolutions++;
                     }
                  }
               }
         } else {
            list[currentLetterIndex] = binaryDict.get(letterList.get(currentLetterIndex))-1;
            currentLetterIndex--;
         }
      }

      double end = System.currentTimeMillis();
      System.out.println(numSolutions + " solutions found");
      System.out.println("time taken : " + (end-start));
      return solutions;
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      if (size < 2) return "1 ";
      for (Character c: uniqueLetters.keySet()) {
         sb.append(c + ":");
         sb.append(uniqueLetters.get(c));
         sb.append (" ");
      }
      return sb.toString();
   }

}











